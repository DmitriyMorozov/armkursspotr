﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DataBaseApi;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using WpfAppGymNext;

namespace WpfAppGymNext
{
    /// <summary>
    /// Логика взаимодействия для SportsmanWindow.xaml
    /// </summary>
    public partial class SportsmanWindow : Window
    {
        private bool isEdit = false;
        private Sportsman? selectedRow;
        private RestClient client;

        public SportsmanWindow(RestClient client)
        {
            InitializeComponent();
            this.client = client;

            updateDataGrid();
        }


        private void SaveBtn_Click(object sender, RoutedEventArgs e)
        {
            Sportsman man = new Sportsman();
            string FIO = FIOTextBox.Text;
            string INN = INNTextBox.Text;

            if (FIO == "" || INN == "" )
            {
                MessageBox.Show("Поля ФИО и ИНН должны быть введены!", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            DateTime? selectedDate = BirthDatePicker.SelectedDate;

            if (selectedDate.HasValue)
            {
                if (!isEdit)
                {
                    man = new Sportsman { FIO = FIO, INN = INN, BirthDate = (DateTime)selectedDate };
                }
                else
                {
                    selectedRow.FIO = FIO;
                    selectedRow.INN = INN;
                    selectedRow.BirthDate = (DateTime)selectedDate;
                }
            }
            else
            {
                MessageBox.Show("Не была выбрана дата рождения!", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (!isEdit)
            {
                client.Post("api/Sportsman", man);

                if (client.IsOk())
                {
                    MessageBox.Show("Запись успешно добавлена!", "Информация", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    dynamic errorContent = JsonConvert.DeserializeObject(client.GetContent());
                    MessageBox.Show(errorContent["errors"].ToString(), "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                client.Put("api/Sportsman", selectedRow);

                if (client.IsOk())
                {
                    MessageBox.Show("Запись успешно изменена!", "Информация", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    dynamic errorContent = JsonConvert.DeserializeObject(client.GetContent());
                    MessageBox.Show(errorContent["errors"].ToString(), "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                }

                isEdit = false;
                EditBtn.IsEnabled = true;
                CancelBtn.Visibility = Visibility.Hidden;
                DeleteBtn.Visibility = Visibility.Visible;
                SportsmanDataGrid.IsEnabled = true;
            }

            FIOTextBox.Text = "";
            INNTextBox.Text = "";

            updateDataGrid();
        }


        private void DeleteBtn_Click(object sender, RoutedEventArgs e)
        {
            if (selectedRow == null)
            {
                return;
            }

            client.Delete("api/Sportsman", selectedRow.Id);

            if (client.IsOk())
            {
                MessageBox.Show("Запись успешно удалена!", "Информация", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                dynamic errorContent = JsonConvert.DeserializeObject(client.GetContent());
                MessageBox.Show(errorContent["errors"].ToString(), "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            updateDataGrid();
            selectedRow = null;
        }


        private void EditBtn_Click(object sender, RoutedEventArgs e)
        {
            if (selectedRow == null)
            {
                return;
            }

            isEdit = true;
            EditBtn.IsEnabled = false;
            CancelBtn.Visibility = Visibility.Visible;
            DeleteBtn.Visibility = Visibility.Hidden;
            SportsmanDataGrid.IsEnabled = false;

            FIOTextBox.Text = selectedRow.FIO;
            INNTextBox.Text = selectedRow.INN;
            BirthDatePicker.SelectedDate = selectedRow.BirthDate;
        }


        private void updateDataGrid()
        {
            SportsmanDataGrid.Items.Refresh();

            SportsmanDataGrid.ItemsSource = client.Get<Sportsman>("api/Sportsman");
        }
        

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            isEdit = false;
            EditBtn.IsEnabled = true;
            CancelBtn.Visibility = Visibility.Hidden;
            DeleteBtn.Visibility = Visibility.Visible;
            SportsmanDataGrid.IsEnabled = true;

            FIOTextBox.Text = "";
            INNTextBox.Text = "";
        }


        private void dataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            selectedRow = SportsmanDataGrid.SelectedItem as Sportsman;
        }
    }
}
