﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DataBaseApi;
using WpfAppGymNext;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace WpfAppGymNext
{
    /// <summary>
    /// Логика взаимодействия для SportsmanWindow.xaml
    /// </summary>
    public partial class ClassTypeWindow : Window
    {
        private bool isEdit = false;
        private ClassType? selectedRow;
        private RestClient client;


        public ClassTypeWindow(RestClient client)
        {
            InitializeComponent();
            this.client = client;

            updateDataGrid();
        }


        private void SaveBtn_Click(object sender, RoutedEventArgs e)
        {
            ClassType type = new ClassType();
            string strType = TypeTextBox.Text;

            if (strType == "")
            {
                MessageBox.Show("Поле Тип должны быть введены!", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (!isEdit)
            {
                type = new ClassType { Name = strType };
                client.Post("api/ClassType", type);

                if (client.IsOk())
                {
                    MessageBox.Show("Запись успешно добавлена!", "Информация", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    dynamic errorContent = JsonConvert.DeserializeObject(client.GetContent());
                    MessageBox.Show(errorContent["errors"].ToString(), "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                selectedRow.Name = strType;
                client.Put("api/ClassType", selectedRow);

                if (client.IsOk())
                {
                    MessageBox.Show("Запись успешно изменена!", "Информация", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    dynamic errorContent = JsonConvert.DeserializeObject(client.GetContent());
                    MessageBox.Show(errorContent["errors"].ToString(), "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                }

                isEdit = false;
                EditBtn.IsEnabled = true;
                CancelBtn.Visibility = Visibility.Hidden;
                DeleteBtn.Visibility = Visibility.Visible;
                ClassTypeDataGrid.IsEnabled = true;
            }

            TypeTextBox.Text = "";

            updateDataGrid();
        }


        private void DeleteBtn_Click(object sender, RoutedEventArgs e)
        {
            if (selectedRow == null)
            {
                return;
            }

            client.Delete("api/ClassType", selectedRow.Id);

            if (client.IsOk())
            {
                MessageBox.Show("Запись успешно удалена!", "Информация", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                dynamic errorContent = JsonConvert.DeserializeObject(client.GetContent());
                MessageBox.Show(errorContent["errors"].ToString(), "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            updateDataGrid();
            selectedRow = null;
        }


        private void EditBtn_Click(object sender, RoutedEventArgs e)
        {
            if (selectedRow == null)
            {
                return;
            }

            isEdit = true;
            EditBtn.IsEnabled = false;
            CancelBtn.Visibility = Visibility.Visible;
            DeleteBtn.Visibility = Visibility.Hidden;
            ClassTypeDataGrid.IsEnabled = false;

            TypeTextBox.Text = selectedRow.Name;
        }


        private void updateDataGrid()
        {
            ClassTypeDataGrid.Items.Refresh();

            ClassTypeDataGrid.ItemsSource = client.Get<ClassType>("api/ClassType");
        }


        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            isEdit = false;
            EditBtn.IsEnabled = true;
            CancelBtn.Visibility = Visibility.Hidden;
            DeleteBtn.Visibility = Visibility.Visible;
            ClassTypeDataGrid.IsEnabled = true;

            TypeTextBox.Text = "";
        }


        private void dataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            selectedRow = ClassTypeDataGrid.SelectedItem as ClassType;
        }
    }
}
