﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DataBaseApi;
using WpfAppGymNext.ViewModels;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Data;

namespace WpfAppGymNext
{
    /// <summary>
    /// Логика взаимодействия для SportsmanWindow.xaml
    /// </summary>
    public partial class GroupWindow : Window
    {
        private bool isEdit = false;
        private GroupViewModel? selectedRow;
        private ClassType? selectedRowType;
        private RestClient client;


        public GroupWindow(RestClient client)
        {
            InitializeComponent();
            this.client = client;

            updateDataGrid();
        }


        private void SaveBtn_Click(object sender, RoutedEventArgs e)
        {
            Group group = new Group();
            string strType = GroupTextBox.Text;

            if (strType == "")
            {
                MessageBox.Show("Поле Тип должны быть введены!", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (selectedRowType != null)
            {
                if (!isEdit)
                {
                    group = new Group { Name = strType, ClassTypeId = selectedRowType.Id };
                    
                    client.Post("api/Group", group);

                    if (client.IsOk())
                    {
                        MessageBox.Show("Запись успешно добавлена!", "Информация", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        dynamic errorContent = JsonConvert.DeserializeObject(client.GetContent());
                        MessageBox.Show(errorContent["errors"].ToString(), "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                else
                {
                    //selectedRow.Name = strType;
                    //selectedRow.ClassTypeId = selectedRowType.Id;

                    group = new Group { Id = selectedRow.Id, Name = strType, ClassTypeId = selectedRowType.Id };

                    client.Put("api/Group", group);

                    if (client.IsOk())
                    {
                        MessageBox.Show("Запись успешно изменена!", "Информация", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        dynamic errorContent = JsonConvert.DeserializeObject(client.GetContent());
                        MessageBox.Show(errorContent["errors"].ToString(), "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                    }

                    isEdit = false;
                    EditBtn.IsEnabled = true;
                    CancelBtn.Visibility = Visibility.Hidden;
                    DeleteBtn.Visibility = Visibility.Visible;
                    GroupDataGrid.IsEnabled = true;
                }
            }
            else
            {
                MessageBox.Show("Поле Тип должен быть выбран!", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }


            GroupTextBox.Text = "";

            updateDataGrid();
        }


        private void DeleteBtn_Click(object sender, RoutedEventArgs e)
        {
            if (selectedRow == null)
            {
                return;
            }

            client.Delete("api/Group", selectedRow.Id);

            if (client.IsOk())
            {
                MessageBox.Show("Запись успешно удалена!", "Информация", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                dynamic errorContent = JsonConvert.DeserializeObject(client.GetContent());
                MessageBox.Show(errorContent["errors"].ToString(), "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            updateDataGrid();
            selectedRow = null;
        }


        private void EditBtn_Click(object sender, RoutedEventArgs e)
        {
            if (selectedRow == null)
            {
                return;
            }

            isEdit = true;
            EditBtn.IsEnabled = false;
            CancelBtn.Visibility = Visibility.Visible;
            DeleteBtn.Visibility = Visibility.Hidden;
            GroupDataGrid.IsEnabled = false;

            foreach (ClassType gridItem in ClassTypeDataGrid.ItemsSource)
            {
                if (gridItem.Id == selectedRow.ClassTypeId)
                {
                    ClassTypeDataGrid.SelectedItem = gridItem;
                    break;
                }
            }

            //ClassTypeDataGrid.SelectedItem = selectedRow.ClassTypeId;

            GroupTextBox.Text = selectedRow.Name;
        }


        private void updateDataGrid()
        {
            GroupDataGrid.Items.Refresh();
            ClassTypeDataGrid.Items.Refresh();

            List<GroupViewModel> viewModel = new List<GroupViewModel>();

            var groups = client.Get<Group>("api/Group");
            var classTypes = client.Get<ClassType>("api/ClassType");

            foreach (var group in groups)
            {
                viewModel.Add(
                    new GroupViewModel 
                    {
                        Id = group.Id,
                        Name = group.Name,
                        ClassTypeId = group.ClassTypeId,
                        ClassTypeName = classTypes.FirstOrDefault(t => t.Id == group.ClassTypeId).Name
                    });
            }
            
            GroupDataGrid.ItemsSource = viewModel;
            ClassTypeDataGrid.ItemsSource = classTypes;
        }


        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            isEdit = false;
            EditBtn.IsEnabled = true;
            CancelBtn.Visibility = Visibility.Hidden;
            DeleteBtn.Visibility = Visibility.Visible;
            //ClassTypeDataGrid.IsEnabled = true;
            GroupDataGrid.IsEnabled = true;


            GroupTextBox.Text = "";
        }


        private void dataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            selectedRow = GroupDataGrid.SelectedItem as GroupViewModel;
        }

        private void dataGridType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            selectedRowType = ClassTypeDataGrid.SelectedItem as ClassType;
        }
    }
}
