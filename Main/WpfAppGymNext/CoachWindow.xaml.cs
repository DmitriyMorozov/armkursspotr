﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DataBaseApi;
using WpfAppGymNext;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace WpfAppGymNext
{
    /// <summary>
    /// Логика взаимодействия для SportsmanWindow.xaml
    /// </summary>
    public partial class CoachWindow : Window
    {
        private bool isEdit = false;
        private Coach? selectedRow;
        private RestClient client;


        public CoachWindow(RestClient client)
        {
            InitializeComponent();
            this.client = client;

            updateDataGrid();
        }


        private void SaveBtn_Click(object sender, RoutedEventArgs e)
        {
            Coach man = new Coach();
            string FIO = FIOTextBox.Text;
            string INN = INNTextBox.Text;

            if (FIO == "" || INN == "" )
            {
                MessageBox.Show("Поля ФИО и ИНН должны быть введены!", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            DateTime? selectedDate = BirthDatePicker.SelectedDate;

            if (selectedDate.HasValue)
            {

                if (!isEdit)
                {
                    man = new Coach { FIO = FIO, INN = INN, BirthDate = (DateTime)selectedDate };
                }
                else
                {
                    selectedRow.FIO = FIO;
                    selectedRow.INN = INN;
                    selectedRow.BirthDate = (DateTime)selectedDate;
                }
            }
            else
            {
                MessageBox.Show("Не была выбрана дата рождения!", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (!isEdit)
            {
                client.Post("api/Coach", man);

                if (client.IsOk())
                {
                    MessageBox.Show("Запись успешно добавлена!", "Информация", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    dynamic errorContent = JsonConvert.DeserializeObject(client.GetContent());
                    MessageBox.Show(errorContent["errors"].ToString(), "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                client.Put("api/Coach", selectedRow);

                if (client.IsOk())
                {
                    MessageBox.Show("Запись успешно изменена!", "Информация", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    dynamic errorContent = JsonConvert.DeserializeObject(client.GetContent());
                    MessageBox.Show(errorContent["errors"].ToString(), "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                }

                isEdit = false;
                EditBtn.IsEnabled = true;
                CancelBtn.Visibility = Visibility.Hidden;
                DeleteBtn.Visibility = Visibility.Visible;
                CoachDataGrid.IsEnabled = true;
            }

            FIOTextBox.Text = "";
            INNTextBox.Text = "";

            updateDataGrid();
        }


        private void DeleteBtn_Click(object sender, RoutedEventArgs e)
        {
            if (selectedRow == null)
            {
                return;
            }

            client.Delete("api/Coach", selectedRow.Id);

            if (client.IsOk())
            {
                MessageBox.Show("Запись успешно удалена!", "Информация", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                dynamic errorContent = JsonConvert.DeserializeObject(client.GetContent());
                MessageBox.Show(errorContent["errors"].ToString(), "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            updateDataGrid();
            selectedRow = null;
        }


        private void EditBtn_Click(object sender, RoutedEventArgs e)
        {
            if (selectedRow == null)
            {
                return;
            }

            isEdit = true;
            EditBtn.IsEnabled = false;
            CancelBtn.Visibility = Visibility.Visible;
            DeleteBtn.Visibility = Visibility.Hidden;
            CoachDataGrid.IsEnabled = false;

            FIOTextBox.Text = selectedRow.FIO;
            INNTextBox.Text = selectedRow.INN;
            BirthDatePicker.SelectedDate = selectedRow.BirthDate;
        }


        private void updateDataGrid()
        {
            CoachDataGrid.Items.Refresh();

            CoachDataGrid.ItemsSource = client.Get<Coach>("api/Coach");
        }


        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            isEdit = false;
            EditBtn.IsEnabled = true;
            CancelBtn.Visibility = Visibility.Hidden;
            DeleteBtn.Visibility = Visibility.Visible;
            CoachDataGrid.IsEnabled = true;

            FIOTextBox.Text = "";
            INNTextBox.Text = "";
        }


        private void dataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            selectedRow = CoachDataGrid.SelectedItem as Coach;
        }
    }
}
