﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DataBaseApi;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using WpfAppGymNext.ViewModels;

namespace WpfAppGymNext
{
    /// <summary>
    /// Логика взаимодействия для SportsmanWindow.xaml
    /// </summary>
    public partial class TimeTableWindow : Window
    {
        private bool isEdit = false;

        private TimeTableViewModel? selectedRow;
        private RoomGym? selectedRoom;
        private Coach? selectedCoach;
        private GroupViewModel? selectedGroup;
        private RestClient client;


        public TimeTableWindow(RestClient client)
        {
            InitializeComponent();
            DateTable.DisplayDateStart = DateTime.Now;

            this.client = client;

            updateDataGrid();
        }


        private void SaveBtn_Click(object sender, RoutedEventArgs e)
        {
            TimeTable timeTable = new TimeTable();

            if (selectedCoach == null || selectedRoom == null || selectedGroup == null)
            {
                MessageBox.Show("Необходимо выбрать Тренера/Помещение/Группа!", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            DateTime? startTime = timePickerStart.Value;
            DateTime? endTime = timePickerEnd.Value;
            DateTime? selectedDate = DateTable.SelectedDate;

            if (startTime.HasValue && endTime.HasValue && selectedDate.HasValue)
            {
                if (!isEdit)
                {
                    timeTable = new TimeTable
                    {
                        Date = (DateTime)selectedDate,
                        RoomGymId = selectedRoom.Id,
                        TimeStart = (DateTime)startTime,
                        TimeEnd = (DateTime)endTime,
                        CoachId = selectedCoach.Id,
                        GroupId = selectedGroup.Id,
                    };
                }
                else
                {
                    timeTable = new TimeTable
                    {
                        Id = selectedRow.Id,
                        Date = (DateTime)selectedDate,
                        RoomGymId = selectedRoom.Id,
                        TimeStart = (DateTime)startTime,
                        TimeEnd = (DateTime)endTime,
                        CoachId = selectedCoach.Id,
                        GroupId = selectedGroup.Id,
                    };
                }
            }
            else
            {
                MessageBox.Show("Не было выбрано время начала/конца/дата!", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (!isEdit)
            {
                bool timeValidation =  crossTimeValidation(timeTable);

                if (timeValidation)
                {
                    client.Post("api/TimeTable", timeTable);

                    if (client.IsOk())
                    {
                        MessageBox.Show("Запись успешно добавлена!", "Информация", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        dynamic errorContent = JsonConvert.DeserializeObject(client.GetContent());
                        MessageBox.Show(errorContent["errors"].ToString(), "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Не верно введено время начала/конца!", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;

                }
            }
            else
            {
                bool timeValidation = crossTimeValidation(timeTable);

                if (timeValidation)
                {
                    client.Put("api/TimeTable", timeTable);

                    if (client.IsOk())
                    {
                        MessageBox.Show("Запись успешно изменена!", "Информация", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        dynamic errorContent = JsonConvert.DeserializeObject(client.GetContent());
                        MessageBox.Show(errorContent["errors"].ToString(), "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Не верно введено время начала/конца!", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;

                }

                isEdit = false;
                EditBtn.IsEnabled = true;
                CancelBtn.Visibility = Visibility.Hidden;
                DeleteBtn.Visibility = Visibility.Visible;
                TimeTableDataGrid.IsEnabled = true;
            }

            updateDataGrid();
        }


        private bool crossTimeValidation(TimeTable tt)
        {
            List<TimeTable> savedData = client.Get<TimeTable>("api/TimeTable").FindAll(el => el != tt);

            foreach (TimeTable time in savedData)
            {
                if ((tt.TimeStart >= time.TimeStart && tt.TimeStart.AddMinutes(1) <= time.TimeEnd) &&
                    tt.RoomGymId == time.RoomGymId &&
                    tt.Date == time.Date &&
                    tt.RoomGymId == time.RoomGymId)
                {
                    return false;
                }
            }

            return true;
        }


        private void DeleteBtn_Click(object sender, RoutedEventArgs e)
        {
            if (selectedRow == null)
            {
                return;
            }

            client.Delete("api/TimeTable", selectedRow.Id);

            if (client.IsOk())
            {
                MessageBox.Show("Запись успешно удалена!", "Информация", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                dynamic errorContent = JsonConvert.DeserializeObject(client.GetContent());
                MessageBox.Show(errorContent["errors"].ToString(), "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            updateDataGrid();
            selectedRow = null;
        }


        private void EditBtn_Click(object sender, RoutedEventArgs e)
        {
            if (selectedRow == null)
            {
                return;
            }

            isEdit = true;
            EditBtn.IsEnabled = false;
            CancelBtn.Visibility = Visibility.Visible;
            DeleteBtn.Visibility = Visibility.Hidden;
            TimeTableDataGrid.IsEnabled = false;

            DateTable.SelectedDate = selectedRow.Date;

            int hourStart = selectedRow.TimeStart.Hour;
            int minuteStart = selectedRow.TimeStart.Minute;

            int hourEnd = selectedRow.TimeEnd.Hour;
            int minuteEnd = selectedRow.TimeEnd.Minute;

            timePickerStart.Value = new DateTime(1, 1, 1, hourStart, minuteStart, 0);
            timePickerEnd.Value = new DateTime(1, 1, 1, hourEnd, minuteEnd, 0);

            foreach (RoomGym gridItem in RoomGymDataGrid.ItemsSource)
            {
                if (gridItem.Id == selectedRow.RoomGymId)
                {
                    RoomGymDataGrid.SelectedItem = gridItem;
                    break;
                }
            }

            foreach (Coach gridItem in CoachDataGrid.ItemsSource)
            {
                if (gridItem.Id == selectedRow.CoachId)
                {
                    CoachDataGrid.SelectedItem = gridItem;
                    break;
                }
            }

            foreach (GroupViewModel gridItem in GroupDataGrid.ItemsSource)
            {
                if (gridItem.Id == selectedRow.GroupId)
                {
                    GroupDataGrid.SelectedItem = gridItem;
                    break;
                }
            }

        }


        private void updateDataGrid()
        {
            TimeTableDataGrid.Items.Refresh();

            List<GroupViewModel> groupViewModel = new List<GroupViewModel>();
            List<TimeTableViewModel> timeTableViewModel = new List<TimeTableViewModel>();

            var timeTables = client.Get<TimeTable>("api/TimeTable");
            var groups = client.Get<Group>("api/Group");
            var rooms = client.Get<RoomGym>("api/RoomGym");
            var coaches = client.Get<Coach>("api/Coach");
            var classTypes = client.Get<ClassType>("api/ClassType");

            foreach (var group in groups)
            {
                groupViewModel.Add(
                    new GroupViewModel
                    {
                        Id = group.Id,
                        Name = group.Name,
                        ClassTypeId = group.ClassTypeId,
                        ClassTypeName = classTypes.FirstOrDefault(t => t.Id == group.ClassTypeId).Name
                    });
            }

            foreach (var timeItem in timeTables)
            {
                timeTableViewModel.Add(
                    new TimeTableViewModel
                    {
                        Id = timeItem.Id,
                        Date = timeItem.Date,
                        RoomGymId = timeItem.RoomGymId,
                        RoomGymName = rooms.FirstOrDefault(r => r.Id == timeItem.RoomGymId).Name,
                        TimeStart = timeItem.TimeStart,
                        TimeEnd = timeItem.TimeEnd,
                        CoachId = timeItem.CoachId,
                        CoachFIO = coaches.FirstOrDefault(c => c.Id == timeItem.CoachId).FIO,
                        GroupId = timeItem.GroupId,
                        GroupName = groups.FirstOrDefault(g => g.Id == timeItem.GroupId).Name
                    });
            }


            GroupDataGrid.ItemsSource = groupViewModel;
            RoomGymDataGrid.ItemsSource = rooms;
            CoachDataGrid.ItemsSource = coaches;
            TimeTableDataGrid.ItemsSource = timeTableViewModel;
        }


        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            isEdit = false;
            EditBtn.IsEnabled = true;
            CancelBtn.Visibility = Visibility.Hidden;
            DeleteBtn.Visibility = Visibility.Visible;
            TimeTableDataGrid.IsEnabled = true;
        }

        private void roomDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            selectedRoom = RoomGymDataGrid.SelectedItem as RoomGym;
        }

        private void coachDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            selectedCoach = CoachDataGrid.SelectedItem as Coach;
        }

        private void dataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            selectedRow = TimeTableDataGrid.SelectedItem as TimeTableViewModel;
        }

        private void groupDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            selectedGroup = GroupDataGrid.SelectedItem as GroupViewModel;
        }
    }
}
