﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DataBaseApi;
using WpfAppGymNext;
using Newtonsoft.Json;
using System.Net.Http.Headers;

namespace WpfAppGymNext
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public RestClient client;

        public MainWindow()
        {
            InitializeComponent();
            client = new RestClient("https://localhost:44303/");

            setButtonsEnable(false);
        }

        private void setButtonsEnable(bool enable)
        {
            SportsmanButton.IsEnabled = enable;
            CoachButton.IsEnabled = enable;
            TypeButton.IsEnabled = enable;
            RoomButton.IsEnabled = enable;
            GroupButton.IsEnabled = enable;
            GroupMembersButton.IsEnabled = enable;
            TimeTableButton.IsEnabled = enable;
            SheduleButton.IsEnabled = enable;
        }

        private void RegButton_Click(object sender, RoutedEventArgs e)
        {
            User user = new User() { Login = LoginTextBox.Text, Password = PasswordTextBox.Password };

            client.Post("auth/Register", user);

            if (client.IsOk())
            {
                MessageBox.Show("Пользователь успешно добавлен!", "Информация", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                MessageBox.Show("Данный пользователь уже существует!", "Информация", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void AuthButton_Click(object sender, RoutedEventArgs e)
        {
            User user = new User() { Login = LoginTextBox.Text, Password = PasswordTextBox.Password };

            client.Post("auth/Login", user);

            if (client.IsOk())
            {
                MessageBox.Show("Пользователь успешно авторизован!", "Информация", MessageBoxButton.OK, MessageBoxImage.Information);
                
                string token = client.GetContent();
                client.SetToken(token);
                setButtonsEnable(true);
            }
            else
            {
                MessageBox.Show("Неверно указан логин или пароль!", "Информация", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void SportsmanButton_Click(object sender, RoutedEventArgs e)
        {
            SportsmanWindow window = new SportsmanWindow(client);
            window.Show();
        }

        private void CoachButton_Click(object sender, RoutedEventArgs e)
        {
            CoachWindow window = new CoachWindow(client);
            window.Show();
        }

        private void TypeButton_Click(object sender, RoutedEventArgs e)
        {
            ClassTypeWindow window = new ClassTypeWindow(client);
            window.Show();
        }

        private void RoomButton_Click(object sender, RoutedEventArgs e)
        {
            RoomGymWindow window = new RoomGymWindow(client);
            window.Show();
        }

        private void GroupButton_Click(object sender, RoutedEventArgs e)
        {
            GroupWindow window = new GroupWindow(client);
            window.Show();
        }

        private void GroupMembersButton_Click(object sender, RoutedEventArgs e)
        {
            GroupMembersWindow window = new GroupMembersWindow(client);
            window.Show();
        }

        private void TimeTableButton_Click(object sender, RoutedEventArgs e)
        {
            TimeTableWindow window = new TimeTableWindow(client);
            window.Show();
        }

        private void SheduleButton_Click(object sender, RoutedEventArgs e)
        {
            SheduleWindow window = new SheduleWindow(client);
            window.Show();
        }
    }
}
