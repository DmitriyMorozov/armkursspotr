﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DataBaseApi;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace WpfAppGymNext
{
    /// <summary>
    /// Логика взаимодействия для SheduleWindow.xaml
    /// </summary>
    public partial class SheduleWindow : Window
    {
        private SortedDictionary<DateTime, List<TimeTable>> sortedData = 
            new SortedDictionary<DateTime, List<TimeTable>>();

        private SortedDictionary<string, List<TimeTable>> sortedDataRooms =
            new SortedDictionary<string, List<TimeTable>>();

        private SortedDictionary<DateTime, List<TimeTable>> sortedDataCoaches =
            new SortedDictionary<DateTime, List<TimeTable>>();

        private List<TimeTable> loadedTimeTable;
        private List<RoomGym> loadedRoomGym;
        private List<Coach> loadedCoach;
        private List<Group> loadedGroup;

        //private RoomGym? selectedRoom;
        private Coach? selectedCoach;
        private RestClient client;

        public SheduleWindow(RestClient client)
        {
            InitializeComponent();
            this.client = client;

            loadedCoach = client.Get<Coach>("api/Coach");
            loadedRoomGym = client.Get<RoomGym>("api/RoomGym");
            loadedGroup = client.Get<Group>("api/Group");
            loadedTimeTable = client.Get<TimeTable>("api/TimeTable");

            sortData();
            sortDataRooms();

            showShedule();

            updateOtherGrid();


        }

        private void updateOtherGrid()
        {
            coachDataGrid.Items.Refresh();
            coachDataGrid.ItemsSource = loadedCoach;
        }

        private void showShedule()
        {
            clearDataGrid();

            List<DateTime> dictKeys = sortedData.Keys.ToList();

            int countCol = 0;
            int maxRow = 0;

            foreach (var key in dictKeys)
            {
                DataGridTextColumn column = new DataGridTextColumn();
                //column.Header = key.ToString().Split(" ")[0];
                column.Header = key.ToShortDateString();
                column.Binding = new Binding($"[{countCol}]");
                //column.Binding.StringFormat = "dd/MM/yyyy";

                if (sortedData[key].Count > maxRow)
                {
                    maxRow = sortedData[key].Count;
                }

                SheduleDataGrid.Columns.Add(column);
                countCol++;
            }

            List<string[]> rows = new List<string[]>();

            for (int i = 0; i < maxRow; i++)
            {
                string[] value = new string[countCol];
                int col = 0;

                foreach (var item in sortedData)
                {
                    if (item.Value.Count > i)
                    {
                        //value[col] = $"{item.Value[i].TimeStart} - {item.Value[i].TimeEnd}\n" +
                        //           $"Группа - {item.Value[i].Group.Name}\n" +
                        //           $"Зал - {item.Value[i].RoomGym.Name}";
                        value[col] = $"{item.Value[i].TimeStart.ToShortTimeString()} - {item.Value[i].TimeEnd.ToShortTimeString()}\n" +
                                    $"Группа - {loadedGroup.FirstOrDefault(g => g.Id == item.Value[i].GroupId).Name}\n" +
                                    $"Зал - {loadedRoomGym.FirstOrDefault(r => r.Id == item.Value[i].RoomGymId).Name}";
                    }

                    col++;
                }

                rows.Add(value);
            }

            SheduleDataGrid.ItemsSource = rows;
        }

        private void clearDataGrid()
        {
            SheduleDataGrid.ItemsSource = null;
            SheduleDataGrid.Columns.Clear();
        }

        private void sortData()
        {
            foreach (var item in loadedTimeTable)
            {
                if (!sortedData.ContainsKey(item.Date))
                {
                    sortedData[item.Date] = new List<TimeTable>(){ item };
                }
                else
                {
                    sortedData[item.Date].Add(item);
                }
            }

            foreach (var item in sortedData)
            {
                if (item.Value.Count > 1)
                {
                    sortedData[item.Key].Sort((x, y) => x.TimeStart.CompareTo(y.TimeStart));
                }
            }
        }

        private void sortDataCoach()
        {
            foreach (var item in loadedTimeTable)
            {
                Coach currentCoach = loadedCoach.FirstOrDefault(c => c.Id == item.CoachId);

                if (currentCoach == selectedCoach)
                {
                    if (!sortedDataCoaches.ContainsKey(item.Date))
                    {
                        sortedDataCoaches[item.Date] = new List<TimeTable>() { item };
                    }
                    else
                    {
                        sortedDataCoaches[item.Date].Add(item);
                    }
                }
            }

            foreach (var item in sortedDataCoaches)
            {
                if (item.Value.Count > 1)
                {
                    sortedDataCoaches[item.Key].Sort((x, y) => x.TimeStart.CompareTo(y.TimeStart));
                }
            }
        }

        private void sortDataRooms()
        {
            foreach (var item in loadedTimeTable)
            {
                string roomName = loadedRoomGym.FirstOrDefault(r => r.Id == item.RoomGymId).Name;

                if (!sortedDataRooms.ContainsKey(roomName))
                {
                    sortedDataRooms[roomName] = new List<TimeTable>() { item };
                }
                else
                {
                    sortedDataRooms[roomName].Add(item);
                }
            }

            foreach (var item in sortedDataRooms)
            {
                if (item.Value.Count > 1)
                {
                    sortedDataRooms[item.Key].Sort((x, y) => x.TimeStart.CompareTo(y.TimeStart));
                }
            }
        }

        
        private void coachDataGrid_SelectionChanged(object sender, RoutedEventArgs e)
        {
            selectedCoach = coachDataGrid.SelectedItem as Coach;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            clearDataGrid();

            List<string> dictKeys = sortedDataRooms.Keys.ToList();

            int countCol = 0;
            int maxRow = 0;

            foreach (var key in dictKeys)
            {
                DataGridTextColumn column = new DataGridTextColumn();
                column.Header = key.ToString();
                column.Binding = new Binding($"[{countCol}]");

                if (sortedDataRooms[key].Count > maxRow)
                {
                    maxRow = sortedDataRooms[key].Count;
                }

                SheduleDataGrid.Columns.Add(column);
                countCol++;
            }

            List<string[]> rows = new List<string[]>();

            for (int i = 0; i < maxRow; i++)
            {
                string[] value = new string[countCol];
                int col = 0;

                foreach (var item in sortedDataRooms)
                {
                    if (item.Value.Count > i)
                    {
                        //value[col] = $"{item.Value[i].TimeStart.ToShortTimeString()} - {item.Value[i].TimeEnd.ToShortTimeString()}\n" +
                        //           $"Группа - {item.Value[i].Group.Name}\n" +
                        //           $"Тренер - {item.Value[i].Coach.FIO}";
                        value[col] = $"{item.Value[i].TimeStart.ToShortTimeString()} - {item.Value[i].TimeEnd.ToShortTimeString()}\n" +
                                   $"Группа - {loadedGroup.FirstOrDefault(g => g.Id == item.Value[i].GroupId).Name}\n" +
                                   $"Тренер - {loadedCoach.FirstOrDefault(c => c.Id == item.Value[i].CoachId).FIO}";
                    }

                    col++;
                }

                rows.Add(value);
            }

            SheduleDataGrid.ItemsSource = rows;
        }

        private void allShedule_Click(object sender, RoutedEventArgs e)
        {
            showShedule();
        }

        private void trainerBtn_Click(object sender, RoutedEventArgs e)
        {
            sortedDataCoaches = new SortedDictionary<DateTime, List<TimeTable>>();
            sortDataCoach();

            clearDataGrid();

            List<DateTime> dictKeys = sortedDataCoaches.Keys.ToList();

            int countCol = 0;
            int maxRow = 0;

            foreach (var key in dictKeys)
            {
                DataGridTextColumn column = new DataGridTextColumn();
                column.Header = key.ToString();
                column.Binding = new Binding($"[{countCol}]");

                if (sortedDataCoaches[key].Count > maxRow)
                {
                    maxRow = sortedDataCoaches[key].Count;
                }

                SheduleDataGrid.Columns.Add(column);
                countCol++;
            }

            List<string[]> rows = new List<string[]>();

            for (int i = 0; i < maxRow; i++)
            {
                string[] value = new string[countCol];
                int col = 0;

                foreach (var item in sortedDataCoaches)
                {
                    if (item.Value.Count > i)
                    {
                        //value[col] = $"{item.Value[i].TimeStart} - {item.Value[i].TimeEnd}\n" +
                        //           $"Группа - {item.Value[i].Group.Name}\n" +
                        //           $"Тренер - {item.Value[i].Coach.FIO}";
                        value[col] = $"{item.Value[i].TimeStart.ToShortTimeString()} - {item.Value[i].TimeEnd.ToShortTimeString()}\n" +
                                   $"Группа - {loadedGroup.FirstOrDefault(g => g.Id == item.Value[i].GroupId).Name}\n" +
                                   $"Тренер - {loadedCoach.FirstOrDefault(c => c.Id == item.Value[i].CoachId).FIO}";
                    }

                    col++;
                }

                rows.Add(value);
            }

            SheduleDataGrid.ItemsSource = rows;

        }
    }
}
