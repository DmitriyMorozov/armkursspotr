﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfAppGymNext.ViewModels
{
    public class TimeTableViewModel
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public int RoomGymId { get; set; }
        public string RoomGymName { get; set; }
        public DateTime TimeStart { get; set; }
        public DateTime TimeEnd { get; set; }
        public int CoachId { get; set; }
        public string CoachFIO { get; set; }
        public int GroupId { get; set; }
        public string GroupName { get; set; }
    }
}