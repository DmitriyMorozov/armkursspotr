﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfAppGymNext.ViewModels
{
    class GroupViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int ClassTypeId { get; set; }
        public string ClassTypeName { get; set; }

    }
}
