﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfAppGymNext.ViewModels
{
    class GroupMembersViewModel
    {
        public int Id { get; set; }
        public int SportsmanId { get; set; }
        public string SportsmanName { get; set; }
        public int GroupId { get; set; }
        public string GroupName { get; set; }
    }
}
