﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DataBaseApi;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query.Internal;
using Newtonsoft.Json;
using WpfAppGymNext.ViewModels;

namespace WpfAppGymNext
{
    /// <summary>
    /// Логика взаимодействия для SportsmanWindow.xaml
    /// </summary>
    public partial class GroupMembersWindow : Window
    {
        private bool isEdit = false;
        
        private GroupMembersViewModel? selectedRow;
        private GroupViewModel? selectedRowGroup;
        private Sportsman? selectedRowSportsman;
        private RestClient client;

        public GroupMembersWindow(RestClient client)
        {
            InitializeComponent();
            this.client = client;

            updateDataGrid();
        }


        private void SaveBtn_Click(object sender, RoutedEventArgs e)
        {
            GroupMembers groupMembers = new GroupMembers();

            if (selectedRowSportsman != null || selectedRowGroup != null)
            {
                if (!isEdit)
                {
                    groupMembers = new GroupMembers { SportsmanId = selectedRowSportsman.Id, GroupId = selectedRowGroup.Id };
                    //db.AddToTable(groupMembers);

                    client.Post("api/GroupMembers", groupMembers);

                    if (client.IsOk())
                    {
                        MessageBox.Show("Запись успешно добавлена!", "Информация", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        dynamic errorContent = JsonConvert.DeserializeObject(client.GetContent());
                        MessageBox.Show(errorContent["errors"].ToString(), "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                else
                {
                    isEdit = false;
                    EditBtn.IsEnabled = true;
                    CancelBtn.Visibility = Visibility.Hidden;
                    DeleteBtn.Visibility = Visibility.Visible;
                    GroupMemberDataGrid.IsEnabled = true;

                    groupMembers.Id = selectedRow.Id;
                    groupMembers.SportsmanId = selectedRowSportsman.Id;
                    groupMembers.GroupId = selectedRowGroup.Id;

                    //db.UpdateTable(selectedRow);
                    client.Put("api/GroupMembers", groupMembers);

                    if (client.IsOk())
                    {
                        MessageBox.Show("Запись успешно изменена!", "Информация", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        dynamic errorContent = JsonConvert.DeserializeObject(client.GetContent());
                        MessageBox.Show(errorContent["errors"].ToString(), "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
            else
            {
                MessageBox.Show("должен быть выбран Спортсмен и Группа!", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }


            updateDataGrid();
        }


        private void DeleteBtn_Click(object sender, RoutedEventArgs e)
        {
            if (selectedRow == null)
            {
                return;
            }

            //db.DeleteFromTable(selectedRow);

            client.Delete("api/GroupMembers", selectedRow.Id);

            if (client.IsOk())
            {
                MessageBox.Show("Запись успешно удалена!", "Информация", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                dynamic errorContent = JsonConvert.DeserializeObject(client.GetContent());
                MessageBox.Show(errorContent["errors"].ToString(), "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            updateDataGrid();
            selectedRow = null;
        }


        private void EditBtn_Click(object sender, RoutedEventArgs e)
        {
            if (selectedRow == null)
            {
                return;
            }

            isEdit = true;
            EditBtn.IsEnabled = false;
            CancelBtn.Visibility = Visibility.Visible;
            DeleteBtn.Visibility = Visibility.Hidden;
            GroupMemberDataGrid.IsEnabled = false;

            foreach (GroupViewModel item in GroupDataGrid.ItemsSource)
            {
                if (item.Id == selectedRow.GroupId)
                {
                    GroupDataGrid.SelectedItem = item;
                    break;
                }
            }

            foreach (Sportsman item in SportsmanDataGrid.ItemsSource)
            {
                if (item.Id == selectedRow.SportsmanId)
                {
                    SportsmanDataGrid.SelectedItem = item;
                    break;
                }
            }

        }


        private void updateDataGrid()
        {
            GroupDataGrid.Items.Refresh();
            GroupMemberDataGrid.Items.Refresh();
            SportsmanDataGrid.Items.Refresh();

            List<GroupMembersViewModel> viewModel = new List<GroupMembersViewModel>();
            List<GroupViewModel> viewGroupModel = new List<GroupViewModel>();

            var groupMembers = client.Get<GroupMembers>("api/GroupMembers");
            var groups = client.Get<Group>("api/Group");
            var sports = client.Get<Sportsman>("api/Sportsman");
            var classTypes = client.Get<ClassType>("api/ClassType");

            foreach (var gMember in groupMembers)
            {
                viewModel.Add(
                    new GroupMembersViewModel
                    {
                        Id = gMember.Id,
                        SportsmanId = gMember.SportsmanId,
                        SportsmanName = sports.FirstOrDefault(s => s.Id == gMember.SportsmanId).FIO,
                        GroupId = gMember.GroupId,
                        GroupName = groups.FirstOrDefault(g => g.Id == gMember.GroupId).Name
                    });
            }

            foreach (var group in groups)
            {
                viewGroupModel.Add(
                    new GroupViewModel
                    {
                        Id = group.Id,
                        Name = group.Name,
                        ClassTypeId = group.ClassTypeId,
                        ClassTypeName = classTypes.FirstOrDefault(t => t.Id == group.ClassTypeId).Name
                    });
            }

            SportsmanDataGrid.ItemsSource = sports;
            GroupDataGrid.ItemsSource = viewGroupModel;
            GroupMemberDataGrid.ItemsSource = viewModel;
        }


        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            isEdit = false;
            EditBtn.IsEnabled = true;
            CancelBtn.Visibility = Visibility.Hidden;
            DeleteBtn.Visibility = Visibility.Visible;
            //ClassTypeDataGrid.IsEnabled = true;
            GroupMemberDataGrid.IsEnabled = true;
        }


        private void dataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            selectedRow = GroupMemberDataGrid.SelectedItem as GroupMembersViewModel;
        }

        private void dataGridSportsman_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            selectedRowSportsman = SportsmanDataGrid.SelectedItem as Sportsman;
        }

        private void dataGridGroup_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            selectedRowGroup = GroupDataGrid.SelectedItem as GroupViewModel;
        }

    }
}
