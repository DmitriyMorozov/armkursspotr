﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBaseApi
{
    public class BaseModel
    {
        [Key]
        public int Id { get; set; }
    }

    public class User : BaseModel
    {

        [Required]
        public string? Login { get; set; }

        [Required]
        public string? Password { get; set; }
    }

    public class Sportsman : BaseModel
    {
        [Required]
        [RegularExpression("^[А-ЯЁ][а-яё]+ [А-ЯЁ][а-яё]+ [А-ЯЁ][а-яё]+$")]
        [MaxLength(50)]
        public string? FIO { get; set; }

        [Required]
        [MaxLength(11)]
        [MinLength(11)]
        [RegularExpression("^[0-9]*$")]
        public string? INN { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime BirthDate { get; set; }
    }

    public class Coach : BaseModel
    {
        [Required]
        [RegularExpression("^[А-ЯЁ][а-яё]+ [А-ЯЁ][а-яё]+ [А-ЯЁ][а-яё]+$")]
        [MaxLength(50)]
        public string? FIO { get; set; }

        [Required]
        [MaxLength(11)]
        [MinLength(11)]
        [RegularExpression("^[0-9]*$")]
        public string? INN { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime BirthDate { get; set; }
    }

    public class ClassType : BaseModel
    {
        [Required]
        [MaxLength(25)]
        public string? Name { get; set; }
    }

    public class Group : BaseModel
    {
        [Required]
        [MaxLength(15)]
        public string? Name { get; set; }

        [Required]
        public int ClassTypeId { get; set; }      // внешний ключ
    }

    public class GroupMembers : BaseModel
    {
        [Required]
        public int SportsmanId { get; set; }      // внешний ключ

        [Required]
        public int GroupId { get; set; }      // внешний ключ
    }

    public class RoomGym : BaseModel
    {
        [Required]
        [MaxLength(25)]
        public string? Name { get; set; }
    }

    public class TimeTable : BaseModel
    {
        [Required]
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        [Required]
        public int RoomGymId { get; set; }      // внешний ключ

        [Required]
        [DataType(DataType.Time)]
        public DateTime TimeStart { get; set; }

        [Required]
        [DataType(DataType.Time)]
        public DateTime TimeEnd { get; set; }

        [Required]
        public int CoachId { get; set; }      // внешний ключ

        [Required]
        public int GroupId { get; set; }      // внешний ключ
    }
}