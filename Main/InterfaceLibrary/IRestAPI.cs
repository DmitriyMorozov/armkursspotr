﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBaseApi
{
    public interface IRestAPI<T>
    {
        Task<IActionResult> List();
        Task<IActionResult> Create(T entity);
        Task<IActionResult> Update(int id, T entity);
        Task<IActionResult> Delete(int id);
        Task<IActionResult> Get(int id);
    }
}
