﻿using DataBaseApi;
using Microsoft.AspNetCore.Mvc;

namespace RestAPINext.Controllers
{
    public class GroupMembersController : ApiController<GroupMembers>
    {
    }

    public class SportsmanController : ApiController<Sportsman>
    {
    }

    public class CoachController : ApiController<Coach>
    {
    }

    public class ClassTypeController : ApiController<ClassType>
    {
    }

    public class GroupController : ApiController<Group>
    {
    }

    public class RoomGymController : ApiController<RoomGym>
    {
    }

    public class TimeTableController : ApiController<TimeTable>
    {
    }
}
